

import os.path

__all__ = [
    "__title__",
    "__summary__",
    "__uri__",
    "__version__",
    "__commit__",
    "__author__",
    "__email__",
    "__license__",
    "__copyright__",
]


try:
    base_dir = os.path.dirname(os.path.abspath(__file__))
except NameError:
    base_dir = None


__title__ = "fingym"
__summary__ = "Reinforcement learning gym for finance domain"
__url__ = "https://gitlab.com/zhu913104/fingym"

__version__ = "0.0.1 dev1"

if base_dir is not None and os.path.exists(os.path.join(base_dir, ".commit")):
    with open(os.path.join(base_dir, ".commit")) as fp:
        __commit__ = fp.read().strip()
else:
    __commit__ = None

__author__ = "toadwei"
__email__ = "toadwei@cathayholdings.com"

__license__ = "Apache License, Version 2.0"
__copyright__ = "2021 %s" % __author__
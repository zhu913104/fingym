# import os
from setuptools import setup,find_packages

requires = [
    "finlab>=0.1.36.dev1",
    "pandas>= 1.2.4"
]

about = {}
with open(file='__about__.py', mode='r', encoding='utf-8') as f:
    exec(f.read(), about)

setup(
    name=about["__title__"],  # 包名称
    version=about["__version__"],  # 包版本
    description=about["__summary__"],  # 包详细描述
    long_description=open('README.md').read(),   # 长描述，通常是readme，打包到PiPy需要
    long_description_content_type='text/markdown',
    author=about["__author__"],  # 作者名称
    author_email=about["__email__"],  # 作者邮箱
    url=about["__url__"],   # 项目官网
    packages=find_packages(),    # 项目需要的包
    # data_files=file_data,   # 打包时需要打包的数据文件，如图片，配置文件等
    # include_package_data=False,  # 是否需要导入静态数据文件
    python_requires=">=3.8",  # Python版本依赖
    install_requires=requires,  # 第三方库依赖
    zip_safe=False,  # 此项需要，否则卸载时报windows error
    classifiers=[    # 程序的所属分类列表
        'Development Status :: 1 - Planning',
        'Programming Language :: Python',
        'Programming Language :: Python :: 3',
    ],
)
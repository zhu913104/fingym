from datetime import datetime , timedelta
import pandas as pd
def CheckandFixDate(date_string:str,df:pd.DataFrame,fix:int):
    while date_string not in df.index:
        date_string = (datetime.strptime(date_string,"%Y-%m-%d")+timedelta(days=fix)).strftime("%Y-%m-%d")
    return date_string

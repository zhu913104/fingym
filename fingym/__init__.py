import configparser
from typing import Tuple
import finlab
from finlab import data
import pandas as pd
from fingym.utils import CheckandFixDate



config = configparser.ConfigParser()

class ENV():
    def __init__(self,config_path) -> None:
        config.read(config_path)
        finlab.login(config.get("DEFAULT","finlab_token"))
        self.__init_cash = config.getfloat("DEFAULT","cash")
        self.__stock_target = eval(config.get("DEFAULT","stock"))
        self.__index = eval(config.get("DEFAULT","index"))
        self.__stateDatafram = [data.get(index) for index in self.__index]
        self.__stockPrice = data.get("price:收盤價")
        self.__start_date =  CheckandFixDate(config.get("DEFAULT","start_date"),self.__stockPrice ,1)
        self.__end_date = CheckandFixDate(config.get("DEFAULT","end_date"),self.__stockPrice ,-1)
        self.__stockPrice = self.__stockPrice.loc[self.__start_date:self.__end_date]
        self.__dateCountEnd = len(self.__stockPrice.index)-1
        self.__reset_variable()

    def __reset_variable(self) -> None:
        # 重置持有現金以及持有股票以及日期
        self.__cash = self.__init_cash
        self.__cash_ = self.__init_cash
        self.__stock_asset = {key:0 for key in self.__stock_target}
        self.__stock_asset_ = {key:0 for key in self.__stock_target}
        self.__date_ = self.__start_date
        self.__done = False
        self.__dateCount = 0

    
    def __update_state(self) -> None:
        # 目前用來剛更新日期
        self.__date_ = self.__stockPrice.iloc[self.__dateCount].name.strftime("%Y-%m-%d")
        # self.__cash = self.__cash_
        # self.__stock_asset = self.__stock_asset_


    def __get_state(self) -> dict:
        #將目標股票以及想要取的指標組成字典後返回
        return {stock: [state.iloc[self.__dateCount][stock] for state in self.__stateDatafram ] for  stock in self.__stock_target}

    def reset(self) -> dict:
        #重置環境至初始化狀態，並且返回初始的state
        self.__reset_variable()
        return self.__get_state()
    
    def __check_action(self,stockID,num) -> bool:
        #確認手上持股是否足夠出售，或是手上現金是否可以購入股票
        return  self.__stock_asset_[stockID]+num>=0 and self.__cash_ - self.__stockPrice.iloc[self.__dateCount][stockID]*num>0

    def __get_total_asset(self,cash:float,strDate:str,stock_asset:dict) -> float:
        """
        cash：持有現金
        strDate："%Y-%m-%d"格式的當前日期
        stock_asset：{股票代碼：持有數量}

        reuturn：當前資產

        以輸入的股價來計算目前資產總額
        """
        for stockID,num in stock_asset.items():
            if pd.isna(self.__stockPrice.iloc[strDate][stockID]):
                continue
            tmp = self.__stockPrice.iloc[strDate][stockID]*num
            cash += tmp

        return cash

    def __caculate_reword(self) -> float:
        #計算純利 
        total_asset = self.__get_total_asset(self.__cash,0,self.__stock_asset)
        total_asset_ = self.__get_total_asset(self.__cash_,self.__dateCount,self.__stock_asset_)
        return (total_asset_/total_asset)-1

    def step(self,action:dict) -> Tuple[dict,float,bool,dict]:
        """
        action:{股票代號:購入股數}

        state:日期更新後的狀態
        reword:獎勵
        self.__done:是否結束
        info：其他相關資訊
        """
        self.__dateCount+=1
        if self.__dateCount >= self.__dateCountEnd or self.__done:
            self.__dateCount-=1
            self.__done=True
        else:
            for stockID,num in action.items():
                #確認是否有足夠庫存可售出股票或是手上現金是否可以購入股票。
                if self.__check_action(stockID,num) :
                    self.__stock_asset_[stockID]+=num
                    self.__cash_ -= self.__stockPrice.iloc[self.__dateCount][stockID]*num
                    action[stockID] = num
                else:
                    #若無法購入股票則返回動作為0
                    action[stockID]=0

        reword = self.__caculate_reword()
        self.__update_state()
        state = self.__get_state()
        total_asset = self.__get_total_asset(self.__cash_,self.__dateCount,self.__stock_asset_)
        info = {"action":action,"stock_asset":self.__stock_asset_,"cash":self.__cash_,"total_asset":total_asset,"date":self.__date_}
         
        return state,reword,self.__done,info
